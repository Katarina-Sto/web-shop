describe('Cart', () => {
  it('Should empty cart', () => {
    cy.visit('/cart');
    cy.get('ng-container').should('have.length', '0')
    cy.contains('Cart is Empty')
    //cy.pause();
  });
  it('Item in cart', ()=>{
    cy.visit('/cart');
    cy.get('[name]').type('Apple MacBook Air 13,3')
    cy.get('ng-container').should('have.length', '1')
  })
});
