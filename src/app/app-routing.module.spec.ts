import {CartComponent} from "./header/cart/cart.component";
import {routes} from "./app-routing.module";
import {HomeComponent} from "./header/home/home.component";
import {ShippingComponent} from "./shipping/shipping.component";

describe('routes', () => {
  it('should contain a route for /cart ', () => {
    expect(routes).toContain({path: 'cart', component: CartComponent});
  });
  it('should contain a route for /home ', () => {
    expect(routes).toContain({path: '', component: HomeComponent});
  });
  it('should contain a route for /shipping ', () => {
    expect(routes).toContain({path: 'shipping', component: ShippingComponent});
  });

});
