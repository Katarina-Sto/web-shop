import {NgModule} from '@angular/core';
import {RouterModule, RouterOutlet, Routes} from '@angular/router';


import { CartComponent } from "./header/cart/cart.component";
import {HomeComponent} from "./header/home/home.component";
import { ShippingComponent } from "./shipping/shipping.component";


export const routes=  [
  {
    path: '',
    component: HomeComponent
  },
    {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'shipping',
    component: ShippingComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule, RouterOutlet]
})
export class AppRoutingModule {
}
