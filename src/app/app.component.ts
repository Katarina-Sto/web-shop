import { Component } from '@angular/core';

import { Item } from './item';
import {FilterSService} from "./filter-s.service";

@Component({
  selector: 'app-root',
  template: '<router-outlet> </router-outlet>'

})
export class AppComponent {
  title = 'web-shop';

  constructor(private _filterSService: FilterSService ) {

  }

}
