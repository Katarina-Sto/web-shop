import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {CartComponent} from './header/cart/cart.component';
import {SearchComponent} from './header/search/search.component';
import {ItemComponent} from './shoping/item/item.component';
import {FilterComponent} from './shoping/filter/filter.component';
import {HeaderComponent} from './header/header/header.component';
import {ShopingComponent} from './shoping/shoping/shoping.component';
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule, routes} from "./app-routing.module";
import {HomeComponent} from './header/home/home.component';
import {SearchPricePipe} from './common/search-price.pipe';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";

import {MatInputModule} from "@angular/material/input";
import {RouterModule} from "@angular/router";
import {MatCardModule} from "@angular/material/card";
import { ShippingComponent } from './shipping/shipping.component';
import {Ng2SearchPipeModule} from "ng2-search-filter";
import {BehaviorSubject} from "rxjs";
import {MatSelectModule} from "@angular/material/select";

@NgModule({
  declarations: [
    AppComponent,
    CartComponent,
    SearchComponent,
    ItemComponent,
    FilterComponent,
    HeaderComponent,
    ShopingComponent,
    HomeComponent,
    SearchPricePipe,
    ShippingComponent

  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatIconModule,
        RouterModule.forRoot(routes),
        MatButtonModule,

        MatInputModule,
        RouterModule.forRoot([
            {path: 'cart', component: CartComponent},
            {path: '', component: ItemComponent},
            {path: 'items/:item.id', component: ItemComponent},
            {path: 'shipping', component: ShippingComponent},
            {path: 'home', component: HomeComponent}
        ]),
        MatCardModule,
        Ng2SearchPipeModule,
        MatSelectModule,


    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {


}
