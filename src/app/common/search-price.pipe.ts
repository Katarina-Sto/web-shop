import {Pipe, PipeTransform} from '@angular/core';
import {Item} from "../item";
import {ISearch} from "../isearch";

@Pipe({
  name: 'search'
})
export class SearchPricePipe implements PipeTransform {

  transform(items: Item[], search: ISearch): Item[] {
    if (!search) return items;

    if (!(search.name === "") && (search.price>0)) {
    return items.filter(item => {
      return item.name?.toLowerCase().startsWith(search.name.toLowerCase()) && search.price >= Number(item.price);
    });
    } else if (!(search.name === "")) {
      return items.filter(item => {
        return item.name?.toLowerCase().startsWith(search.name.toLowerCase());
      });
    }
    return items.filter(item => {
      return search.price >= Number(item.price);
    });
  }

}


// return item.kategorija?.toLowerCase() === item.kategorija
