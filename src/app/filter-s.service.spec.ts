import { TestBed } from '@angular/core/testing';

import { FilterSService } from './filter-s.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";

describe('FilterSService', () => {
  let service: FilterSService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule],
    });
    service = TestBed.inject(FilterSService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
