import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartComponent } from './cart.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {By} from "@angular/platform-browser";
import {ItemSService} from "../../service/item-s.service";
import {observable, Observable} from "rxjs";
import { EMPTY } from "rxjs";
import {provideRoutes} from "@angular/router";

describe('CartComponent', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;
  let service: ItemSService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],

      }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartComponent ],

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should click Delete button', () => {
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   const getForm = fixture.debugElement.query(By.css('#form'));
  //   expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();
  // });

  it('should  call the component', ()=>{
    spyOn(window, 'confirm').and.returnValue(true);
    let  spy = spyOn(component, 'removeItem').and.returnValue();

    component.removeItem(1);
    expect(spy).toHaveBeenCalledOnceWith(1);


  });




  // it('should NOT call the component ', ()=>{
  //   spyOn(window, 'confirm').and.returnValue(false);
  //   let spy = spyOn(component, 'removeItem').and.returnValue();
  //
  //   component.removeItem(1);
  //   expect(spy).not.toHaveBeenCalled();
  //
  // })

});


