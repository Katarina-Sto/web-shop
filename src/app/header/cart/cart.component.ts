import {Component, OnInit} from '@angular/core';
import {CartService} from "../../../service-cart/cart.service";
import {Item} from "../../item";
import {ItemSService} from "../../service/item-s.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  items: Item[] = [];
  public total: number = 0;

  product = this.cartService.getProduct(); //dodavanje artikla u korpu
  constructor(private cartService: CartService) {
  }

  ngOnInit(): void {
    this.total = this.cartService.getTotalPrice();
  }

  removeItem(item: any){
    this.cartService.removeCartItem(item);
  }


}
