import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {HeaderComponent} from './header.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {By} from "@angular/platform-browser";
import {DebugElement} from "@angular/core";
import {ItemSService} from "../../service/item-s.service";
//import {RouterOutlet} from "@angular/router";
import {RouterOutlet, RouterLinkWithHref } from "@angular/router";


describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  //let submitbtn: DebugElement;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes([])],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    //submitbtn= fixture.debugElement.query(By.css('.submit-btn'))

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should click Submit button', () => {
  //   spyOn(component, 'onSubmit');
  //   let form = fixture.debugElement.nativeElement.querySelector('#form');
  //   let button = form.querySelector('.submit-btn');
  //   fixture.detectChanges();
  //   expect(button).toBeTruthy();
  //
  // });


  it('should click Submit button', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const getForm = fixture.debugElement.query(By.css('#form'));
    expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();
  });

  it('form valid when empty', () => {
    expect(component.form.valid).toBeTruthy();
  });

  it('should have a router outlet', ()=>{
    let de =  fixture.debugElement.query(By.directive(RouterOutlet));

    expect(de).not.toBeNull();
  });

  it('should have a link to cart page', ()=>{
   let debbugElement =  fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));

   // href="/cart"
   let index = debbugElement.findIndex(de => de.properties['href'] === '/cart');
   expect(index).toBeGreaterThanOrEqual(-1);   /**toBeGreater??????*/
  });



});
