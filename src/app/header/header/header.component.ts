import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ISearch} from "../../isearch";
import {Item} from "../../item";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public item: Item | undefined;
  form: FormGroup;
  public filterCategory: any;
  @Output() searchIt = new EventEmitter<ISearch>();


  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      'price': [],
      'name': [""],
      'category': [""],
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.searchIt.emit(this.form.value);
  }

  refresh(): void {
    window.location.reload();
  }

}
