import {Component, OnInit} from '@angular/core';
import {ISearch} from "../../isearch";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // @ts-ignore
  search: ISearch;

  constructor() {
  }

  ngOnInit(): void {
  }

  searchIt(value: ISearch) {
    this.search = value;
  }
}
