export interface ISearch {
  id: string,
  price: number,
  name: string,
  image: string,
  kategorija?: string
}
