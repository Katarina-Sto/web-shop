export interface Item {
  id?: string,
  name?: string,
  detail?: string,
  price?: number,
  hero?: string,
  image?: string,
  kategorija?: string
}
