import { TestBed } from '@angular/core/testing';

import { ItemSService } from './item-s.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";

describe('ItemSService', () => {
  let service: ItemSService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule],
    });
    service = TestBed.inject(ItemSService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getAllItems should return real value', () => {
    expect(service.getAllItems()).toBeTruthy();
  });


});
