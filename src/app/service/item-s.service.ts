import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Item} from "../item";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ItemSService {

  constructor(private http: HttpClient) {

  }

  getAllItems() {
    return this.http.get<Item[]>(`${environment.api}/computerList`).toPromise();
  }

  getAllCategory() {
    return this.http.get<string[]>(`${environment.api}/category`).toPromise();
  }
}
