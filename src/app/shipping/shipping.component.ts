import { Component, OnInit } from '@angular/core';

import { CartService } from "../../service-cart/cart.service";
import {Item} from "../item";


@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.css']
})
export class ShippingComponent implements OnInit {

  public shippingCosts:any;

  constructor(private cartServivce: CartService ) { }

  //shippingCosts = this.cartService.getShippingPrices();

  ngOnInit(): void {
  }

}
