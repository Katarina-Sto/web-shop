import {Component, Input, OnInit} from '@angular/core';
import {Item} from "../../item";
import {ItemSService} from "../../service/item-s.service";
import {ActivatedRoute} from "@angular/router";
import {CartService} from "../../../service-cart/cart.service";
import {ISearch} from "../../isearch";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  items: Item[] = [];
  categorys: string[] = [];

  // @ts-ignore
  @Input() search: ISearch;

  constructor(private service: ItemSService, private route: ActivatedRoute, private cartService: CartService) {
  }

  ngOnInit(): void {
    this.getAllItems();
    this.getAllCategory();
  }

  addToCart(item: Item) {
    this.cartService.addToCart(item);
    alert("Proivod je u korpi" + item.name);
  }

  private getAllItems() {
    this.service.getAllItems().then(data => {
      this.items = data as Item[];
    });
  }

  private getAllCategory(){
    this.service.getAllCategory().then(data =>{
      this.categorys = data as string[];
    });

  }


}
