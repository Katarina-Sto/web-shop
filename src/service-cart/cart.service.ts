import {Injectable} from '@angular/core';
import {Item} from "../app/item";
import {BehaviorSubject} from "rxjs";

import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {ISearch} from "../app/isearch";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  products: Item[] = [];
  shippingCosts: Item[] = [];




  constructor(
    private http: HttpClient
  ) {
  }

  getShippingPrices() {

    // return this.http.get<Item[]>(`${environment.api}/computerList`).toPromise();
    return this.http.get<{ type: string, price: number }[]>(`${environment.api}/computerList`);
  }

  addToCart(item: Item) {
    this.products.push(item)
  }

  getProduct() {
    return this.products;
  }


  getTotalPrice(): number {
    let total: number = 0;
    this.products.map((a: Item) => {
      total += Number(a.price ?? 0);
    })
    return total;
  }


  removeCartItem(product: any){
    this.products.map((a:any, index:any)=>{
      if(product.id === a.id){
        this.products.splice(index, 1);
      }
      }
    )
  }
}
